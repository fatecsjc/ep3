# EP 3

## **Curso:** Tecnologia em Banco de Dados


### **Disciplina:** Estrutura de Dados
**Ano:** 2020 
**Semestre:** 3

**Professor:** Fernando Masanori Ashicaga

**Aluno:** Robson de Sousa

### Descrição do Problema


**EP3 (duplas) – Identificando Imagens Binárias – ED FATEC SJC**

Dada uma imagem binária identifique suas regiões conexas utilizando estruturas de dados do Python. A conexão não ocorre na diagonal, somente na horizontal e vertical. Serão corrigidos apenas os programas que passarem pelos três testes abaixo. Não ler arquivos, atribuir as matrizes como um texto multilinha e depois você irá converter para matriz de zeros e uns. É recomendado testar com outras matrizes de entrada.

Há várias aplicações interessantes para este problema, tais como: aplicações militares (reconhecimento de instalações ou objetos em imagens de satélite), aplicações da agroindústria (previsão de safras) ou na medicina (identificação de regiões cancerígenas em imagens de tomografia). É dada uma imagem através de uma matriz de “pixels”, onde cada pixel pode assumir o valor 0 (branco) ou 1 (preto). Desejamos identificar os objetos conexos da imagem, ou seja, dada a matriz da imagem, seu programa deverá rotular os “pixels” da matriz de forma a identificar os objetos conexos nela contidos. Seu programa deverá ser bastante eficiente, evitando percorrer toda a matriz de entrada várias vezes. Uma estratégia que deverá ser implementada é a que descrevemos a seguir. A matriz é varrida de cima para baixo e da esquerda para a direita. Se um “pixel” tem valor 1, verificamos seu vizinho de cima e o da esquerda. Se ambos são 0, um novo objeto começa neste pixel. Se um deles é 1, este pixel pertence ao objeto anterior. Finalmente, se ambos os vizinhos têm rótulos diferentes, a matriz deverá ser corrigida para que ambos tenham o mesmo rótulo (ou seja, um dos rótulos vai desaparecer e todos os pixels daquele rótulo vão passar a ter o outro). A fim de realizar eficientemente esta última operação cada objeto deverá manter uma lista com as coordenadas dos pixels associados com seu rótulo.

Aviso: A dupla que copiar ou emprestar o EP será reprovada. Caso você troque alguma ideia com um colega, faça isso apenas verbalmente e de forma genérica. Certifique-se de construir o seu EP desde o zero, pois começar a programar a partir de algum código pronto, de outra dupla, de um veterano, ou achado na internet, configura plágio. Alguns sites serão utilizados para verificação automática de plágio. Portanto, não peça o programa de outra dupla, você estará prejudicando colegas se for constatada a cópia. Além disso, não faça seu código baseado em programas de veteranos, todos eles fazem parte da biblioteca pessoal do professor e serão comparados com o EP entregue.