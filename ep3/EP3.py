entrada1 = '''010 111 000 101'''.split()
entrada2 = '''10101 10101 11111'''.split()
entrada3 = '''0011001010 0110001010 0011001110 0000000000 0010001010 
0010011111 1111100000 0010001110 0010001110'''.split()

entrada4 = '''01111 11110 00100 10101 01111'''.split()  # matriz quadrada (opcional)

saida1 = '''010 111 000 203'''.split()
saida2 = '''10101 10101 11111'''.split()
saida3 = '''0011002020 0110002020 0011002220 0000000000 0030004040 
0030044444 3333300000 0030005550 0030005550'''.split()

saida4 = '''02222 22220 00200 10202 02222'''.split()

entradas = [entrada1, entrada2, entrada3, entrada4]
saidas = [saida1, saida2, saida3, saida4]


def geradora(matriz):
    print("#########################################################################\n")

    matriz = [list(indice) for indice in matriz]

    for indice in matriz: print(' '.join(indice))
    print()

    regioes = []
    linha = len(matriz)
    col = len(matriz[0])  # tamanho da coluna == para toda linha n

    def cima_esq(x, y):
        if x == -1 or y == -1:
            return False
        return matriz[x][y] == '1'

    def regindex(x, y):
        for regi in range(len(regioes)):
            for coord in regioes[regi]:
                if (x, y) == coord:
                    return regi
        else:
            return -1

    for j in range(linha):
        for k in range(col):
            if matriz[j][k] == '1':
                rcima = resq = -1
                if cima_esq(j - 1, k):
                    rcima = regindex(j - 1, k)
                if cima_esq(j, k - 1):
                    resq = regindex(j, k - 1)
                if rcima != -1:
                    regioes[rcima].append((j, k))
                if resq != -1:
                    if rcima != -1:
                        if rcima == resq:
                            continue
                        regioes[rcima].extend(regioes[resq])

                        del regioes[resq]
                    else:
                        regioes[resq].append((j, k))

                if rcima == resq == -1:
                    regioes.append([(j, k)])

    cont = ord('1')

    for regiao in regioes:
        for j, k in regiao:
            matriz[j][k] = chr(cont)
        cont += 1

    for line in matriz:
        print(' '.join(line))

    print("\n")

    return [''.join(line) for line in matriz]


# print(matrizes=[geradora(mat) for mat in matrizes])


def test(indice, esperado, obtido):
    if esperado == obtido:
        prefixo = 'Parabéns, mas independente do resultado, continue a programar.'
    else:
        prefixo = 'Continue a programar...'
    print("#########################################################################")
    print("Teste %d \n%s \nesperado: %s \nobtido: %s" % (indice + 1, prefixo, repr(esperado), repr(obtido)))


def main():
    print("Geradora")
    # test(0, saida1, geradora(entrada1))

    for indice, valor in enumerate(saidas):
        test(indice, saidas[indice], geradora(entradas[indice]))


if __name__ == '__main__':
    main()
